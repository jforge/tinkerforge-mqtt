[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17642606%2Fissues_statistics)](https://gitlab.com/containerizeme/mqtt/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17642606%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/containerizeme/mqtt/-/commits/master)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17642606%3Flicense%3Dtrue)](https://gitlab.com/containerizeme/mqtt/-/blob/master/LICENSE)
applies to software of this project. The running software within the image/container ships with its own license(s).

[![Mosquitto](https://badgen.net/badge/project/Mosquitto/orange?icon=gitlab)](https://gitlab.com/containerizeme/mqtt/-/blob/master/README.md#mosquitto)
[![Stable Version](https://img.shields.io/docker/v/icebear8/mosquitto/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/mqtt/-/blob/master/CHANGELOG.md#mosquitto)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/mosquitto?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/mosquitto)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/mosquitto/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/mosquitto)

[![Tinkerforge](https://badgen.net/badge/project/Tinkerforge/orange?icon=gitlab)](https://gitlab.com/containerizeme/mqtt/-/blob/master/README.md#tinkerforge)
[![Stable Version](https://img.shields.io/docker/v/icebear8/tinkerforge/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/mqtt/-/blob/master/CHANGELOG.md#tinkerforge)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/tinkerforge?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/tinkerforge)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/tinkerforge/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/tinkerforge)

# Mosquitto
[Mosquitto](https://mosquitto.org/) is an open source implementation of a server for version 5.0, 3.1.1, and 3.1 of the MQTT protocol. It also includes a C and C++ client library, and the mosquitto_pub and mosquitto_sub utilities for publishing and subscribing.

# Usage
`docker run -p 1883:1883 icebear8/mosquitto`

Mosquitto MQTT broker is configured to **allow connections on all interfaces and to allow anonyumous access**. Following configuration parameters are set in this image:
* `listener 1883`
* `allow_anonymous true`

##  Environment Variables

| Variable        | Description |
|-                |-            |
| SERVICE_ARGS    | Arguments for the mosquitto service at startup |

##  Available volumes
| VOLUME              | Description |
|-                    |-            |
| ${APP_RUNTIME_DIR}  | Contains the Mosquitto (`mosquitto.conf`) runtime configuration and data (introduced with v2.0.14-r2) |

# Tinkerforge
[Tinkerforge MQTT](https://www.tinkerforge.com/en/doc/Software/API_Bindings_MQTT.html) allows to control Bricks and Bricklets using the MQTT protocol.
This image does only contain the Tinkerforge MQTT translation proxy.
It does **NOT** contain an MQTT broker or Tinkerforge daemon.

# Usage
`docker run icebear8/tinkerforge`

##  Environment Variables

| Variable        | Description |
|-                |-            |
| SERVICE_ARGS    | Arguments for the Tinkerforge MQTT proxy service at startup |

##  Connect to Daemon and Broker
The connection to the MQTT broker and the Tinkerforge daemon are provided by the environment variable.
The settings for host accept IPs, host names and URLs.
The Tinkerforge WiFi module can directly be accessed as host.

`docker run -e SERVICE_ARGS='--ipcon-host <tinkerforgeHost> --broker-host <mqttBrokerHost>' icebear8/tinkerforge`
